package api.redis;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import redis.clients.jedis.Jedis;

/**
 * Created by Слава on 03.05.2017.
 */
public class DBUtils {

    private static Jedis jedis = null;
    private static MongoClient mongoClient = null;
    private static final String dataBaseName = "SoundRecordStudio";

//    private final String host = "127.0.0.1";
    private final String host = "192.168.1.200";
    private final Integer port = 27017;

    public Jedis createRedisConnection () {
        try {
            jedis = new Jedis(host);
            System.out.println("Connection to redis server successfully");
        } catch(Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }

        return jedis;
    }

    public MongoClient createMongoConnection () {
        try {
            mongoClient = new MongoClient(host, port);
            System.out.println("Connection to mongo server successfully");
        } catch(Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }

        return mongoClient;
    }

    public static Jedis getRedisConnection () {
        return jedis;
    }

    public static MongoDatabase getMongoDataBase () {
        return mongoClient.getDatabase(dataBaseName);
    }

    public static MongoCollection getMongoCollection (String collectionName) {
        return mongoClient.getDatabase(dataBaseName).getCollection(collectionName);
    }
}
