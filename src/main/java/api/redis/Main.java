package api.redis;

import java.util.List;

/**
 * Created by Слава on 04.04.2017.
 */
public class Main {

    private static String operation = null;
    private static String collectionName = null;
    private static String what = null;
    private static String[] params = null;

    public static void main(String[] args) {
        DBUtils dbUtils = new DBUtils();

        dbUtils.createMongoConnection();
        dbUtils.createRedisConnection();

        System.out.println("Connected to databases success.");

        parseArgs(args);

        changeOperation();
    }

    private static void parseArgs (String[] args) {
        operation = args.length > 0 ? args[0] : "Empty operation";
        if(operation.equals("get") || operation.equals("del")) {
            collectionName = "Albums";
            what = args.length > 1 ? args[1] : "Empty what";
            params = args.length > 2 ? new String[args.length - 2] : new String[0];

            if(args.length > 2) {
                System.arraycopy(args, 2, params, 0, params.length);
            }
        } else {
            collectionName = args.length > 1 ? args[1] : "Empty collectionName";
            what = args.length > 2 ? args[2] : "Empty what";
            params = args.length > 3 ? new String[args.length - 3] : new String[1];
            if(args.length > 3) {
                System.arraycopy(args, 3, params, 0, params.length);
            }
        }


    }

    private static void changeOperation () {
        Operations operations = new Operations(collectionName);

        switch (operation) {
            case "set":
                changeSetWhat(operations);
                break;
            case "get":
                changeGetWhat(operations);
                break;
            case "del":
                operations.del(what);
                break;
            default:
                System.err.println("Unknown command");
        }
    }

    private static void changeSetWhat (Operations operations) {

        switch (what) {
            case "all":
                operations.setFromMongoAll();
                break;
            case "top":
                if (params.length > 0) {
                    operations.setFromMongoGetTopAlbums(params[0]);
                } else {
                    System.err.println("Error! Need more args");
                }
                break;
            case "byKey":
                if (params.length > 2) {
                    operations.setFromMongoByKey(params[0], params[1], params[2]);
                } else {
                    System.err.println("Error! Need more args");
                }
                break;
            default:
                System.err.println("Unknown command for set");
                break;
        }
    }

    private static void changeGetWhat (Operations operations) {

        if (params.length > 0) {
            switch (what) {
                case "byKey":
                    System.out.println(operations.getOneFromRedis(params[0]));
                    break;
                case "array":
                    List<String> arr = operations.getArrayFromRedis(params[0]);
                    for (String str : arr) {
                        System.out.println(str);
                    }
                    break;
                default:
                    System.err.println("Unknown command for get");
                    break;
            }
        } else {
            System.err.println("Error! Need more args");
        }
    }
}
