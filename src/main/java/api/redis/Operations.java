package api.redis;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;
import redis.clients.jedis.Jedis;

import java.util.List;

/**
 * Created by Слава on 03.05.2017.
 */
public class Operations {

    private final Jedis jedis;
    private final MongoCollection mongoCollection;
    private final String collectionName;

    public Operations (String collectionName) {
        jedis = DBUtils.getRedisConnection();
        mongoCollection = DBUtils.getMongoCollection(collectionName);
        this.collectionName = collectionName;
    }

    public void setFromMongoAll () {
        try{
            FindIterable iter = mongoCollection.find();
            MongoCursor<Document> cur = iter.iterator();
            while (cur.hasNext()) {
                Document doc = cur.next();
                if (doc.getString("name") != null) {
                    jedis.set(doc.getString("name"), doc.toJson());
                }
            }

        }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }

    public void setFromMongoGetTopAlbums (String limit) {
        if (collectionName.equals("Albums")) {
            try {
                Bson bson = new BasicDBObject("popularity", -1);

                FindIterable iter = mongoCollection.find().sort(bson).limit(Integer.parseInt(limit));
                MongoCursor<Document> cur = iter.iterator();
                while (cur.hasNext()) {
                    Document doc = cur.next();
                    jedis.lpush("top" + limit, doc.toJson());
                }

            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
            }
        } else {
            System.err.println("This collection: " + collectionName + "don't support get top");
        }
    }

    public String getOneFromRedis (String key) {
       return jedis.get(key);
    }

    public List<String> getArrayFromRedis (String key) {
        Long len = jedis.llen(key);

        return jedis.lrange(key,0,len - 1);
    }

    public void setFromMongoByKey (String key, String field, String value) {
        try {
            FindIterable iter = mongoCollection.find(Filters.eq(field, value));
            MongoCursor<Document> cur = iter.iterator();

            if(cur.hasNext()) {
                Document doc = cur.next();
                System.out.println(doc.toJson());
                jedis.set(key, doc.toJson());
            }

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void del (String key) {
        jedis.del(key);
    }
}
