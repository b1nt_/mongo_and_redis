package api.mongo;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Слава on 01.05.2017.
 */
public class DBUtils {
    private static MongoClient mongoClient = null;
    private static String dataBaseName = "SoundRecordStudio";

//    private final String host = "127.0.0.1";
    private final String host = "192.168.1.200";
    private final Integer port = 27017;

    public MongoClient createConnection() {
        try {
            mongoClient = new MongoClient(host, port);
        } catch (Exception e) {
            System.err.println("Error in create connection");
        }
        return mongoClient;
    }

    public static MongoDatabase getDatabase() {
        return mongoClient.getDatabase(dataBaseName);
    }

    public static MongoCollection getCollection(String collectionName) {
        return mongoClient.getDatabase(dataBaseName).getCollection(collectionName);
    }

    public static Date parseDate(String value) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date = null;
        try {
            date = sdf.parse(value);
        } catch (ParseException e) {
            try {
                date = sdf1.parse(value);
            } catch (ParseException e1) {
                System.err.println("Error in parse date");
            }
        }

        return date;
    }
}
