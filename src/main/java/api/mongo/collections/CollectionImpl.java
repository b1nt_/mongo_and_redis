package api.mongo.collections;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;
import api.mongo.DBUtils;
import org.bson.types.ObjectId;

import java.util.ArrayList;

/**
 * Created by Слава on 01.05.2017.
 */
public class CollectionImpl implements CollectionOperation {

    protected final String collectionName;
    protected final MongoCollection stCollection;

    public CollectionImpl(String collectionName) {
        this.collectionName = collectionName;
        stCollection = DBUtils.getCollection(collectionName);
    }

    public ArrayList<Document> find (String field, String value) {
        ArrayList<Document> documents = new ArrayList<>();
        try {
            FindIterable iter;
            if (value != null && field != null) {
                if (field.equals("_id")) {
                    ObjectId objectId = new ObjectId(value);
                    iter = stCollection.find(Filters.eq(field, objectId));
                } else {
                    iter = stCollection.find(Filters.eq(field, value));
                }
            } else {
                iter = stCollection.find();
            }

            MongoCursor<Document> cur = iter.iterator();
            while (cur.hasNext()) {
                Document doc = cur.next();
                documents.add(doc);
            }
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".find\n" + e);
        }

        return documents;
    }

    public String insert (Document document) {
        try {
                stCollection.insertOne(document);
                MongoCursor<Document> cur = stCollection.find(Filters.eq("name", document.getString("name"))).iterator();
                if (cur.hasNext()) {
                    Document doc = cur.next();
                    return doc.getObjectId("_id").toString();
                }
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".insert\n" + e);
        }
        return null;
    }

    public void delete (Bson bson) {
        try {
            stCollection.deleteOne(bson);
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".delete\n" + e);
        }
    }

    public void update (Bson searchQuery, Document newDocument) {
        try {
            stCollection.findOneAndReplace(searchQuery, newDocument);
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".update\n" + e);
        }
    }

    public String incPopularity (String albumName) {
        try {
            DBUtils.getDatabase().runCommand(new Document("$eval", "db.loadServerScripts()"));

            Document popularity = DBUtils.getDatabase().runCommand(new Document("$eval", "incPopularity('" + albumName + "')"));
            return popularity.toJson();
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".incPopularity\n" + e);
        }
        return null;
    }

    public String decPopularity (String albumName) {
        try {
            DBUtils.getDatabase().runCommand(new Document("$eval", "db.loadServerScripts()"));

            Document popularity = DBUtils.getDatabase().runCommand(new Document("$eval", "decPopularity('" + albumName + "')"));
            return popularity.toJson();
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".incPopularity\n" + e);
        }
        return null;
    }
}
