package api.mongo.collections;

import api.mongo.DBUtils;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.Date;

/**
 * Created by Слава on 30.05.2017.
 */
public class Albums extends CollectionImpl {

    private final static String collectionName = "Albums";

    public Albums() {
        super(collectionName);
    }

    @Override
    public String insert(Document document) {
        try {
            Date release_date = DBUtils.parseDate(document.getString("release_date"));
            if (release_date != null) {
                document.put("release_date", release_date);
            } else {
                document.put("release_date", new Date());
            }
            stCollection.insertOne(document);

            MongoCursor<Document> cur = stCollection.find(Filters.eq("name", document.getString("name"))).iterator();
            if (cur.hasNext()) {
                Document doc = cur.next();
                return doc.getObjectId("_id").toString();
            }
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".insert\n" + e);
        }
        return null;
    }

    @Override
    public void update (Bson searchQuery, Document newDocument) {
        try {
            Date release_date = DBUtils.parseDate(newDocument.getString("release_date"));
            if (release_date != null) {
                newDocument.put("release_date", release_date);
            } else {
                newDocument.put("release_date", new Date());
            }
            stCollection.findOneAndReplace(searchQuery, newDocument);
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".update\n" + e);
        }
    }
}
