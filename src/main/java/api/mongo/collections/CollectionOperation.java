package api.mongo.collections;

import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;

/**
 * Created by Слава on 01.05.2017.
 */
public interface CollectionOperation {
    ArrayList<Document> find (String field, String value);
    String insert (Document document);
    void delete (Bson bson);
    void update (Bson searchQuery, Document newDocument);
}
