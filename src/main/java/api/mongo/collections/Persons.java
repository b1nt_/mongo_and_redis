package api.mongo.collections;

import api.mongo.DBUtils;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Слава on 30.05.2017.
 */
public class Persons extends CollectionImpl {

    private final static String collectionName = "Persons";

    public Persons() {
        super(collectionName);
    }

    @Override
    public String insert(Document document) {
        try {
            Document person_info = (Document) document.get("person_info");
            Date birthday = DBUtils.parseDate(person_info.getString("birthday"));

            if (birthday != null) {
                person_info.put("birthday", birthday);
            } else {
                person_info.put("birthday", new Date());
            }
            document.put("person_info", person_info);
            stCollection.insertOne(document);
            MongoCursor<Document> cur = stCollection.find(Filters.eq("person_info.name", person_info.getString("name"))).iterator();
            Document doc = null;
            while (cur.hasNext()) {
                doc = cur.next();
            }
            if(doc != null)
                return doc.getObjectId("_id").toString();
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".insert\n" + e);
        }
        return null;
    }

    @Override
    public void update (Bson searchQuery, Document newDocument) {
        try {
            Document person_info = (Document) newDocument.get("person_info");
            Date birthday = DBUtils.parseDate(person_info.getString("birthday"));

            if (birthday != null) {
                person_info.put("birthday", birthday);
            } else {
                person_info.put("birthday", new Date());
            }
            newDocument.put("person_info", person_info);
            stCollection.findOneAndReplace(searchQuery, newDocument);
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".update\n" + e);
        }
    }
}
