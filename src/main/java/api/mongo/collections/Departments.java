package api.mongo.collections;

import api.mongo.DBUtils;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Слава on 30.05.2017.
 */
public class Departments extends CollectionImpl {

    private final static String collectionName = "Departments";

    public Departments() {
        super(collectionName);
    }

    @Override
    public String insert(Document newDocument) {
        try {
            ArrayList<Document> contracts =(ArrayList<Document>) newDocument.get("contracts");
            for (Document doc: contracts) {
                Date start_date = DBUtils.parseDate(doc.getString("start_date"));
                Date end_date = DBUtils.parseDate(doc.getString("end_date"));

                if (start_date != null && end_date != null) {
                    doc.put("start_date", start_date);
                    doc.put("end_date", end_date);
             } else {
                    doc.put("start_date", new Date());
                    doc.put("end_date", new Date(new Date().getTime() + (1000 * 60 * 60 * 24)));
                }
            }
            newDocument.put("contracts", contracts);
            stCollection.insertOne(newDocument);
            MongoCursor<Document> cur = stCollection.find(Filters.eq("name", newDocument.getString("name"))).iterator();
            if (cur.hasNext()) {
                Document doc = cur.next();
                return doc.getObjectId("_id").toString();
            }
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".insert\n" + e);
        }
        return null;
    }

    @Override
    public void update (Bson searchQuery, Document newDocument) {
        try {
            ArrayList<Document> contracts = (ArrayList<Document>) newDocument.get("contracts");
            for (Document doc: contracts) {
                Date start_date = DBUtils.parseDate(doc.getString("start_date"));
                Date end_date = DBUtils.parseDate(doc.getString("end_date"));

                if (start_date != null && end_date != null) {
                    doc.put("start_date", start_date);
                    doc.put("end_date", end_date);
                } else {
                    doc.put("start_date", new Date());
                    doc.put("end_date", new Date(new Date().getTime() + (1000 * 60 * 60 * 24)));
                }
            }
            newDocument.put("contracts", contracts);
            stCollection.findOneAndReplace(searchQuery, newDocument);
        } catch (Exception e) {
            System.err.println("Error in "+ collectionName +".update\n" + e);
        }
    }
}
