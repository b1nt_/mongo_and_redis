package api.mongo;

import api.mongo.collections.Albums;
import api.mongo.collections.CollectionImpl;
import api.mongo.collections.Departments;
import api.mongo.collections.Persons;

/**
 * Created by Слава on 01.05.2017.
 */
public class Main {

    private static String operation;
    private static String collectionName;
    private static String[] params;

    public static void main(String[] args) {
        DBUtils dbUtils = new DBUtils();
        dbUtils.createConnection();
        System.out.println("Connected to database success.");

        parseArgs(args);
        CollectionImpl collection = changeCollection();
        changeOperation(collection);
    }

    private static void parseArgs (String[] args) {
        operation = args.length > 0 ? args[0] : "Empty operation";
        collectionName = args.length > 1 ? args[1] : "Empty collectionName";
        params = args.length > 2 ? new String[args.length - 2] : new String [3];

        if(args.length > 2) {
            System.arraycopy(args, 2, params, 0, params.length);
        }
    }

    private static CollectionImpl changeCollection() {
        CollectionImpl collection = null;

        switch (collectionName) {
            case "Albums":
                collection = new Albums();
                break;
            case "Departments":
                collection = new Departments();
                break;
            case "Persons":
                collection = new Persons();
                break;
            case "Studios":
                collection = new CollectionImpl(collectionName);
                break;
        }

        if (collection == null) {
            System.err.println("Error! Unknown collection name: " + collectionName);
            System.exit(1);
        }

        return collection;
    }

    private static void changeOperation(CollectionImpl collection) {
        OperationsImpl operationsImpl = new OperationsImpl(collection);
        switch (operation) {
            case "find" :
                operationsImpl.find(params[0], params[1]);
                break;
            case "insert":
                operationsImpl.insert(params[0]);
                break;
            case "update":
                operationsImpl.update(params[0], params[1], params[2]);
                break;
            case "delete":
                operationsImpl.delete(params[0], params[1]);
                break;
            case "incPopularity":
                operationsImpl.incPopularity(params[0]);
                break;
            case "decPopularity":
                operationsImpl.decPopularity(params[0]);
                break;
            default: System.err.println("Error! Unknown command: " + operation);
        }
    }
}
