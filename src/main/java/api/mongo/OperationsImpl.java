package api.mongo;

import api.mongo.collections.CollectionImpl;
import com.mongodb.BasicDBObject;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.ArrayList;

/**
 * Created by Слава on 01.05.2017.
 */
public class OperationsImpl {

    private CollectionImpl collection;

    public OperationsImpl (CollectionImpl collection) {
        this.collection = collection;
    }

    public void find (String field, String value) {
        if (field == null || value == null) {
            System.err.println("Error. Args are null");
            System.exit(1);
        }

        ArrayList<Document> documents = collection.find(field, value);

        for(Document doc : documents) {
            System.out.println(doc);
        }
    }

    public void insert (String value) {
        if (value == null) {
            System.err.println("Error. Args are null");
            System.exit(1);
        }

        Document doc = Document.parse(value);
        String objectId = collection.insert(doc);
        if(objectId != null) {
            System.out.println(objectId);
        }
    }

    public void delete (String key, String value) {
        if (key == null || value == null) {
            System.err.println("Error. Args are null");
            System.exit(1);
        }

        Bson bson;
        if (key.equals("_id")) {
            ObjectId objectId = new ObjectId(value);
            bson = new BasicDBObject(key, objectId);
        } else {
            bson = new BasicDBObject(key, value);
        }

        collection.delete(bson);
    }

    public void update (String key, String value, String newDocument) {
        if (key == null || value == null || newDocument == null) {
            System.err.println("Error. Args are null");
            System.exit(1);
        }

        Bson bson;
        if (key.equals("_id")) {
            ObjectId objectId = new ObjectId(value);
            bson = new BasicDBObject(key, objectId);
        } else {
            bson = new BasicDBObject(key, value);
        }

        collection.update(bson, Document.parse(newDocument));
    }

    public void incPopularity (String albumName) {
        if (albumName == null) {
            System.err.println("Error. Args are null");
            System.exit(1);
        }
        CollectionImpl collection = new CollectionImpl("Albums");

        System.out.println(collection.incPopularity(albumName));
    }

    public void decPopularity (String albumName) {
        if (albumName == null) {
            System.err.println("Error. Args are null");
            System.exit(1);
        }
        CollectionImpl collection = new CollectionImpl("Albums");

        System.out.println(collection.decPopularity(albumName));
    }

}
